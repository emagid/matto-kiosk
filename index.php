<?php 

// error_reporting(-1);
ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

if (session_status() == PHP_SESSION_NONE) {
    \session_start();
}

  

require_once("vendor/autoload.php");
require_once("libs/Emagid/emagid.php");
require_once("conf/emagid.conf.php");
require_once('includes/functions.php');
require_once('templates/notification_template.php');
//require_once('libs/authorize/AuthorizeNet_Config.php');
//require_once('libs/Services/PaymentService.php');
$emagid = new \Emagid\Emagid($emagid_config);

$emagid->loadMvc($site_routes);
