<div class='content'>

  <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
  <section class='main projects_page'>
    <div class='page_title'>
      <p class='sml'>What we've created</p>
      <p class='lrg'>POPSHAP PROJECTS</p>
    </div>

    <div class='projs'>
      <div data-page='project' data-name='Mayo Clinic' data-kiosk='http://mayoclinic1.popshap.net/' class='card proj' style="background-color: #003da5;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/mayo.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/mayo.png">
      </div>
      <div data-page='project' data-name='Red Door Spa' data-table='true' data-kiosk='https://reddoor.popshap.net/' class='card proj' style="background-color: #e40505;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/reddoor.jpg');"></div>
        <img style="max-width: 340px !important" src="<?= FRONT_ASSETS ?>img/reddoor.png">
      </div>
      <div data-page='project' data-name='BCG Consulting' data-kiosk='https://bcg.popshap.net/check_in/bcg-holiday-party' class='card proj' style="background-color: #197a56;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/bcg.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/bcg.png">
      </div>
      <div data-page='project' data-name='CBMA' data-kiosk='https://cbma.popshap.net/' class='card proj' style="background-color: #C9AA6D;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/cbma.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/cbma.png">
      </div>
      <div data-page='project' data-name='Nielsen' data-kiosk='http://nielsenmusic.popshap.net/' class='card proj' style="background-color: #00aeef;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/nielsen.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/nielsen.png">
      </div>
      <div data-page='project' data-name='Cognizant' data-kiosk='http://cognizant.popshap.net/' class='card proj' style="background-color: #023aab;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/cognizant.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/cognizant.png">
      </div>
      <div data-page='project' data-name='Citi Bank' data-kiosk='https://citi-cab.popshap.net/' class='card proj' style="background-color: #056dae;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/citi.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/citi.png">
      </div>
      <div data-page='project' data-name='Bike Expo New York' data-kiosk='http://beny2.mymagid.net/' class='card proj' style="background-color: #4ab678;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/beny.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/beny.png">
      </div>
      <div data-page='project' data-table='true' data-name='Green Fig' data-kiosk='https://greenfig.popshap.net/' class='card proj' style="background-color: #7dc952;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/greenfig.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/greenfig.png">
      </div>
      <div data-page='project' data-table='true' data-name='Winbow' data-kiosk='https://event.popshap.net/check_in/vintners-harvest' class='card proj' style="background-color: #002d72;">
        <div style="background-image: url('<?= FRONT_ASSETS ?>img/winebow.jpg');"></div>
        <img src="<?= FRONT_ASSETS ?>img/winebow.png">
      </div>
    </div>
  </section>

</div>