<div class='content'>
  <div class='homepage'>
    <video src='<?= FRONT_ASSETS ?>img/vid.mov' type="video/mov" autoplay muted loop></video>
    
    <div class='links'>
      <div id='about' class='link'>
        <img src="<?= FRONT_ASSETS ?>img/hand.png">
        <p>About Matto</p>
      </div>
      <div id='franchise' class='link'>
        <img src="<?= FRONT_ASSETS ?>img/hand.png">
        <p>Start a Matto Franchise</p>
      </div>
      <div id='locations' class='link'>
        <img src="<?= FRONT_ASSETS ?>img/hand.png">
        <p>Matto Locations</p>
      </div>
      <div id='photobooth' class='link'>
        <img src="<?= FRONT_ASSETS ?>img/hand.png">
        <p>Photobooth</p>
      </div>
      <div id='wheel' class='link'>
        <img src="<?= FRONT_ASSETS ?>img/hand.png">
        <p>Spin to Win</p>
      </div>
    </div>
  </div>

  
  <!-- <section class='main home_page'>
    <div class='page_title'>
      <p class='sml'>Welcome to Himms 2019</p>
      <p class='lrg'>POPSHAP PROVIDES<br>TOUCH KIOSK SOLUTIONS</p>
    </div>

    <div data-page='medical' class='click card full click super'>
      <div class='color_slide'></div>
      <p class='sml'>Find out how Popshap can help</p>
      <p class='lrg'>MEDICAL SOLUTIONS</p>
    </div>

    <div class='projs'>
      <div data-page='wheel' class='click card wof'>
        <p class='lrg'>SPIN TO WIN</p>
        <img src="<?= FRONT_ASSETS ?>img/wheelFull.png">
      </div>
      <div data-page='photobooth' class='click card selfie'>
        <p class='lrg'>TAKE A SELFIE</p>
        <img src="<?= FRONT_ASSETS ?>img/confetti3.png">
      </div>
    </div>

    <div data-page='projects' class='click card full project_card'>
      <div class='color_slide'></div>
      <p class='sml'>See what we’ve accomplished</p>
      <p class='lrg'>POPSHAP PROJECTS</p>
    </div>
  </section> -->

</div>