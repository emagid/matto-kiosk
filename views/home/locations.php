<div class='content'>

  <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
  <div class='contentpage locationpage'>
    <div class='locations'>
      <div class='location'>
        <p class='name'>washington square</p>
        <p class='address'>293 Mercer st @ 8th St, 10003</p>
        <p class='hrs'>open: monday - sunday</p>
      </div>
      <div class='location'>
        <p class='name'>chelsea</p>
        <p class='address'>188 7th Ave @ 21st St, 10011</p>
        <p class='hrs'>open: monday - sunday</p>
      </div>
      <div class='location'>
        <p class='name'>midtown west</p>
        <p class='address'>153 W 27th St, 10011</p>
        <p class='hrs'>open: monday - sunday</p>
      </div>
      <div class='location'>
        <p class='name'>garment district</p>
        <p class='address'>252 W 37th St, 10018</p>
        <p class='hrs'>open: monday - friday</p>
      </div>
      <div class='location'>
        <p class='name'>murray hill</p>
        <p class='address'>114 E 40th St @ Park Ave, 10016</p>
        <p class='hrs'>open: monday - friday</p>
      </div>
      <div class='location'>
        <p class='name'>turtle bay</p>
        <p class='address'>844 2nd Ave @ 45th St, 10017</p>
        <p class='hrs'>open: monday - friday</p>
      </div>
      <div class='location'>
        <p class='name'>times square</p>
        <p class='address'>8 W 46th St @ 5th Ave, 10017</p>
        <p class='hrs'>open: monday - friday</p>
      </div>
      <div class='location'>
        <p class='name'>lenox hill</p>
        <p class='address'>359 E 68th St @ 1st Ave, 10065</p>
        <p class='hrs'>open: monday - sunday</p>
      </div>
      <div class='location'>
        <p class='name'>upper west side</p>
        <p class='address'>530 Columbus @ 85th St, 10024</p>
        <p class='hrs'>open: monday - sunday</p>
      </div>
      <div class='location'>
        <p class='name'>washington heights</p>
        <p class='address'>3495 Broadway @ 143rd St, 10031</p>
        <p class='hrs'>open: monday - sunday</p>
      </div>
      <div class='location'>
        <p class='name'>williamsburg</p>
        <p class='address'>150 Grand St @ Bedford, Brooklyn, 10031</p>
        <p class='hrs'>open: monday - friday</p>
      </div>
    </div>
  </div>

</div>