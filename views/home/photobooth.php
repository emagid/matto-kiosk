<div class='content'>
  <link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."photobooth.css")?>">

  <section class='main photobooth_page'>
    <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
    <!-- Step 1 -->
    <div class='step_one'>
      <div class='background' style="background-image: url('<?=FRONT_ASSETS?>img/back.jpg')"></div>
      <img id='filter' src="<?=FRONT_ASSETS?>img/filter.png">
        <video id="video"  autoplay></video>
            <device type="media" onchange="update(this.data)"></device>
            <script>
                function update(stream) {
                    document.querySelector('video').src = stream.url;
                }

                var video = document.getElementById('video');

                if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                        try {
                          video.src = window.URL.createObjectURL(stream);
                        } catch(error) {
                          video.srcObject = stream;
                        }
                        video.play();
                    });
                }

                $(document).on('click', function(){
                  var video = document.getElementById('video');

                if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                        try {
                          video.src = window.URL.createObjectURL(stream);
                        } catch(error) {
                          video.srcObject = stream;
                        }
                        video.play();
                    });
                }
                })
            </script>

      <div id='take_pic' class='button'>
        <p class='lrg'>Take a selfie</p>
      </div>
        <div class='countdown'>3</div>
        <div class='countdown'>2</div>
        <div class='countdown'>1</div>
    </div>

    <div class='flash'></div>


    <!-- Step 2 -->
    <div id='pictures' class='step_two'>
      <div class='btns'>
        <div class='share button'>
          <p class='lrg'>Share</p>
        </div>
        <div class='retake button white_button'>
          <p class='lrg'>Retake</p>
        </div>
      </div>
    </div>


    <!-- Step 3 -->
    <div class='step_three'>
      <div class='card full'>
        <i class="fa fa-close sharex" style="font-size:36px"></i>
        <form id='submit_form'>
          <input type='hidden' name='form' value="1">
            <input type="hidden" name="image" class="image_encoded">
            <span>
                <input class='input jQKeyboard first_email' name='email[]' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
            </span>
            <p id='add_email'>Add an email +</p>

            <span>
                <input class='input jQKeyboard first_phone' name="phone[]" placeholder="Phone Number (eg: +19874563210)">
            </span>
            <p id='add_phone'>Add a phone number +</p>
        </form>
      </div>

      <div id='share_btn' class='button'>
        <p class='lrg'>Send Picture</p>
      </div>
    </div>

  </section>
        <!-- Alerts -->
  <section id='share_alert' style="background-image: url('<?= FRONT_ASSETS ?>img/thankyou.jpg');">
  </section>

  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.4.6/fabric.min.js'></script>
  <script src="<?=auto_version(FRONT_JS."photobooth.js")?>"></script>
</div>


  


