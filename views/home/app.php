<div class='content'>
	
  <style type="text/css">
  	header {
  		display: none !important;
  	}
  </style>
  <div class='apppage'>
	  <section class='screens'>
	  	<div class='screen red'>
	  		<p class='sml'>Download the app and</p>
	  		<h1>Skip the line!</h1>
	  		<div class='qr'>
		  		<img src="<?= FRONT_ASSETS ?>img/qr.png">
		  		<p>Open your phone camera and scan the qr to donwload the app.</p>
	  		</div>
	  	</div>

	  	<div class='screen gold'>
	  		<p class='sml'>Download the app and</p>
	  		<h1>Order from anywhere!</h1>
	  		<div class='qr'>
		  		<img src="<?= FRONT_ASSETS ?>img/qr.png">
		  		<p>Open your phone camera and scan the qr to donwload the app.</p>
	  		</div>
	  	</div>
	  	<div class='screen white'>
	  		<p class='sml'>Download the app and</p>
	  		<h1>Earn rewards!</h1>
	  		<div class='qr'>
		  		<img src="<?= FRONT_ASSETS ?>img/qr.png">
		  		<p>Scan this qr with your phone to download the app.</p>
	  		</div>
	  	</div>
	  </section>
	  <div class='iphone'>
	  	<p class='script'>Touch below for a demo!</p>
	  	<img class='phone_img' src="<?= FRONT_ASSETS ?>img/iphone.png">
	  	<div class='route route_home'></div>
	  	<div class='routes'>
	  		<div data-id='route1' class='route1 route'></div>
	  		<div data-id='route2' class='route2 route'></div>
	  		<div data-id='route3' class='route3 route'></div>
	  		<div data-id='route4' class='route4 route'></div>
	  	</div>

	  	<div class='step_one step active'>
	  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic1.png">
	  	</div>

  		<div class='app_screen' id='route1'>
		  	<div class='step_two step active'>
		  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic2.png">
		  	</div>
		  	<div class='step_three step'>
		  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic3.png">
		  	</div>
		  	<div class='step_four step'>
		  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic4.png">
		  	</div>
		  	<div class='step_five step'>
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic5.png">
		  	</div>
  		</div>
  		<div class='app_screen' id='route2'>
		  	<div class='step_loc step active'>
		  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic6.png">
		  	</div>
		  	<div class='step_five step'>
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic7.png">
		  	</div>
  		</div>
  		<div class='app_screen' id='route3'>
		  	<div class='step_two step active'>
		  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic2.png">
		  	</div>
		  	<div class='step_three step'>
		  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic3.png">
		  	</div>
		  	<div class='step_four step'>
		  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic4.png">
		  	</div>
		  	<div class='step_five step'>
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic5.png">
		  	</div>
  		</div>
  		<div class='app_screen' id='route4'>
		  	<div class='step_five step active'>
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic8.png">
		  	</div>
  		</div>
	  </div>
  </div>

  <script type="text/javascript">
  	slide($('.screen')[0]);
	var timer;
	var touchTimer;

	function slide(s){
		$(s).fadeIn(1000);
		$(s).addClass('show');
		var nxt = $(s).next('.screen');
		if ( nxt.length < 1 ) {
			nxt = $('.screen')[0];
		}
		isWhite(s);
		timer = setTimeout(function(){
			$(s).fadeOut(1000);
			$(s).removeClass('show');
			slide(nxt);
		}, 15000);
	}


	function isWhite(s){
		if ( $(s).hasClass('white') ) { 
			$('.iphone .script').addClass('black');
		 }else {
		 	$('.iphone .script').removeClass('black');
		 }
	}

	$(document).on('click', '.route', function(){
		var id = '#'+$(this).attr('data-id');
		var div = $($(id).children('.step')[0]);

		nextStep();
		$(id).fadeIn(300);
		div.fadeIn(300)
	});	

	$(document).on('click', '.route_home', function(){
		reset();
	});	

	function nextStep(){
		$('.routes').hide();
		$('.step_one').fadeOut(300);
	}	

	$(document).on('click', '.step', function(){
		if ( $(this).hasClass('step_five') ) {
			reset();
		}else {
			$(this).fadeOut(300)
			$(this).next('div').fadeIn(300).addClass('active');
			$(this).removeClass('active');
		}
	});

	function reset(s){
		$('.routes').show();
		$('.step_one').fadeIn(300);
		$('.app_screen').fadeOut(300);
		$('.app_screen .step').removeClass('active').hide();
		setTimeout(function(){
			$('.app_screen').each(function(){
				$($(this).children('.step')[0]).addClass('active').show();
			});
		}, 300);
	}

    function invoke() {
        touchTimer = window.setTimeout(
            function() {
                reset($($('.active')[0]));
            }, 30000);
    }

    invoke();

    $('body').on('click mousemove', function(){
        window.clearTimeout(touchTimer);
        invoke();
    });
  </script>

</div>