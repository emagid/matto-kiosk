<style type="text/css">
	header {
		display: none;
	}

	.right-side {
		padding: 0;
	}

	body.fixed .wrapper {
		padding: 0 !important;
		margin: 0 !important;
	}

	.form-box {
	    position: fixed;
	    height: 100%;
	    margin: 0;
	    background: white;
	    padding: 0;
	    top: 0;
	    left: 0;
	    width: 40%;
	    display: flex;
	    justify-content: center;
	    align-items: center;
	    flex-direction: column;
	}

	#admin_login {
	    max-width: 400px;
	    width: 90%;
	    background-color: white;
	}

	.logo_img {
	    max-width: 100px !important;
	    width: 50%;
	    margin-bottom: 60px;
	}

	.bg-gray {
		background-color: white !important;
	}

	.form-box .body > .form-group > input, .form-box .footer > .form-group > input {
		    border-bottom: 1px solid #eaeaea;
		    padding-bottom: 23px;
	}

	input:-webkit-autofill {
	    -webkit-box-shadow: 0 0 0 30px white inset;
	}

	button {
		width: 100%;
	}

	.bg_works {
		background-image: url(/content/frontend/assets/img/login_hero.jpg);
	}

	@media screen and (max-width: 1200px) { 
	  .form-box {
  	    width: 90%;
	    height: auto;
	    position: relative;
	    margin: auto;
	    max-width: 500px;
	    padding: 50px 30px;
	  }

	  .bg_works {
	  	display: flex;
	  }

	}
</style>


<div class="form-box" id="login-box">
	<img class='logo_img' src="/content/frontend/assets/img/logo.png">

	<?php if(isset($model->errors)){ ?>
	<?php foreach ($model->errors as $error) { ?>
	<div class="alert alert-danger"><?php echo $error; ?></div>
	<?php } ?>
	<?php } ?>
	<?php if(isset($model->message)){ ?>
	<p><?php echo $model->message; ?></p>
	<?php } ?>
<!--	<div class="header">ADMIN</div>-->
	<form id="admin_login" action="<?= ADMIN_URL ?>login" method="post">
		<div class="body bg-gray">
			<div class="form-group">
				<input type="text" name="username" class="form-control" placeholder="Username"/>
			</div>
			<div class="form-group">
				<input type="password" name="password" class="form-control" placeholder="Password"/>
			</div>

		</div>
		<div class="footer">
			<button type="submit" class="reverse">Sign in</button>
		</div>
	</form>
</div>

<?php echo footer(); ?>

<script type="text/javascript">
	$("#admin_login").validate({
		rules: {
			username:{
				required: true,
			},
			password:{
				required: true,
			}
		},
		messages:{
			username:{ 
				required:"Please enter your username",
			},
			password:{ 
				required:"Please enter your password",
			}
		},
		errorClass: "error"
	}
	)
</script>