<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
	  	<li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a></li> 
	</ul>
	<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
		<input type="hidden" name="id" value="<?php echo $model->video->id;?>" /> 
		<div class="tab-content">
      		<div role="tabpanel" class="tab-pane active" id="account-info-tab">
          		<div class="row">
              		<div class="col-md-24">
                  		<div class="box">
							<h4>Account Information</h4>
							<div class="form-group">
								<label>Title</label>
								<?php echo $model->form->editorFor('title');?>
							</div>
							<div class="form-group">
								<label>Background Color</label>
								<?php echo $model->form->textBoxFor('background_color',['class'=>'jscolor']);?>
							</div>
							<div class="form-group">
								<label>Video Type</label>
								<?php echo $model->form->dropdownListFor('page',\Model\Video::$types);?>
							</div>
							<div class="form-group" id="banner_order_cont">
								<label>Display Order</label>
								<input id="display_order" type="number" name="display_order" value="<?=$model->video->display_order?>"/>
							</div>
							<div class="form-group">
								<label>Thumbnail</label>
								<p><small>(ideal profile photo size is 850 x 850)</small></p>
								<p><input type="file" name="image" class='image' /></p>
								<div style="display:inline-block">
									<?php $img_path = ""; if($model->video->image != "" && file_exists(UPLOAD_PATH.'videos'.DS.$model->video->image)){ 
											$img_path = UPLOAD_URL . 'videos/' . $model->video->image; ?>
										<div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
											<img src="<?php echo $img_path; ?>" />
											<br />
											<a href="<?= ADMIN_URL.'videos/delete_image/'.$model->video->id;?>?image=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
											<input type="hidden" name="image" value="<?=$model->video->image?>" />
										</div>
									<?php } ?>
									<div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
								</div>
							</div>
							<div class="form-group">
								<label>Video</label>
								<p><small>(.mov,.mp4 files accepted)</small></p>
								<p><input type="file" name="video" class='image' accept=".mov,.mp4" /></p>
								<div style="display:inline-block">
									<?php $img_path = ""; if($model->video->video != "" && file_exists(UPLOAD_PATH.'videos'.DS.$model->video->video)){ 
											$img_path = UPLOAD_URL . 'videos/' . $model->video->video; ?>
										<div class="well well-sm pull-left">
											<video height="100px" src="<?=$img_path; ?>"></video>
											<br />
											<a href="<?= ADMIN_URL.'videos/delete_image/'.$model->video->id;?>?type=video" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
											<input type="hidden" name="video" value="<?=$model->video->video?>" />
										</div>
									<?php } ?>
									<div class='preview-container'></div>
								</div>
							</div>
                  		</div>
              		</div>
              		 
          		</div>
        	</div>
        	 
        	 
        	 
				 
			</div>
        </div>
        <div class="form-group">
			<button type="submit" class="btn btn-save">Save</button>
		</div>
     </form>
</div>

<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<?php footer();?>

<script>

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		var img = $("<img />");
		img.attr('alt','Uploaded Image');
		
		img.attr("width",'100%');
		img.attr('height','100%');
		if($(input).attr('name') == "video"){
			img = $("<video ></video>");
			img.attr('alt','Uploaded Video');
			img.attr('height','100px');
		}
		reader.onload = function (e) {
			img.attr('src',e.target.result);
		};
		$(input).parent().parent().find('.preview-container').html(img);
		$(input).parent().parent().find('input[type="hidden"]').remove();

		reader.readAsDataURL(input.files[0]);
	}
}

$(function(){
	$("select.multiselect").each(function(i,e) {
        //$(e).val('');
        var placeholder = $(e).data('placeholder');
        $(e).multiselect({
            nonSelectedText:placeholder,
            includeSelectAllOption: true,
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%'
        });
    });

	$("input.image").change(function(){
		readURL(this);
	});
});

$('input[name=featured_id]').on('keyup',function(){
	if($(this).val() != 0 || $.trim($(this).val()) == ''){
		$('#banner_order_cont').hide();
		$('#banner_order').prop('disabled',true);
	} else {
		$('#banner_order_cont').show();
		$('#banner_order').prop('disabled',false);
	}
});

$('input[name=featured_id]').trigger('keyup')

</script>