<?php if(count($model->kiosks)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="30%">Name</th> 
          <th width="30%">Location/Description</th> 
          <th width="20%">Initializtion Link</th> 
          <th width="15%" class="text-center">Edit</th>	
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->kiosks as $kiosk): ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>kiosks/update/<?php echo $kiosk->id; ?>"><?php echo $kiosk->name; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>kiosks/update/<?php echo $kiosk->id; ?>"><?php echo $kiosk->description; ?></a></td>
         <td><a href="http://<?= $_SERVER['HTTP_HOST']."/initialize/$kiosk->id" ?>"><?= $_SERVER['HTTP_HOST']."/initialize/$kiosk->id" ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>kiosks/update/<?php echo $kiosk->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>kiosks/delete/<?php echo $kiosk->id; ?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
     <?php endforeach; ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'kiosks/index';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;

</script>

