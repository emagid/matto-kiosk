<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->jewelry->id;?>" />
    <input type="hidden" name="currency_code" value="<?=$model->jewelry->currency_code?>" />
    <input type="hidden" name="metals" value="<?=$model->jewelry->metals?>" />

    <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
      <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
      <li role="presentation"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li>
      <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
     
      
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general-tab">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>

                    <div class="form-group">
                        <label>Featured</label>
                        <?php echo $model->form->checkBoxFor("featured", 1); ?>
                    </div>

                    <div class="form-group" style="<?=$model->jewelry->featured?'':'display:none'?>">
                        <label>Discount %</label>
                        <?php echo $model->form->editorFor("discount"); ?>
                    </div>
                    
                    <div class="form-group">
                      <label>Images</label>
                      <p><small></small></p>
<!--                      <p><input type="file" name="images" class='image' /></p>-->
                      <div style="display:inline-block">
                      <?php $img = new \Model\Product_Image();
                      if($img->getProductImage($model->jewelry->id, 'jewelry')){
                      $images = $img->getProductImage($model->jewelry->id, 'jewelry');
                      foreach($images as $image){
                        $img_path = "";
                        if($image != "" && file_exists(UPLOAD_PATH.'products'.DS.$image->image)){
                            $img_path = UPLOAD_URL . 'products/' . $image->image;
                      ?>
	                      <div class="well well-sm pull-left">
	                          <img src="<?php echo $img_path; ?>" width="100" />
	                          <br />
<!--                              <a href="--><?//= ADMIN_URL.'jewelry/delete_image/'.$model->jewelry->id;?><!--?type=images" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>-->
	                      </div>
                      <?php } ?>
                      <?php } ?>
                      <?php } ?>
                      <div class='preview-container'></div>
                      </div>
                  </div>

<!--                    <div class="form-group">-->
<!--                        <label>Slug</label>-->
<!--                        --><?php //echo $model->form->editorFor("slug"); ?>
<!--                    </div>-->
                    <div class="form-group" style="display:none">
                        <label>Price</label>
                        <?php echo $model->form->editorFor("price"); ?>
                    </div>
                    <div class="form-group">
                        <label>14kt Price</label>
                        <?php echo $model->form->editorFor("total_14kt_price"); ?>
                    </div>
                    <div class="form-group">
                        <label>18kt Price</label>
                        <?php echo $model->form->editorFor("total_18kt_price"); ?>
                    </div>
                    <div class="form-group">
                        <label>Platinum Price</label>
                        <?php echo $model->form->editorFor("total_plat_price"); ?>
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <?php echo $model->form->editorFor("quantity"); ?>
                    </div>
                    <div class="form-group">
                        <label>Materials</label>
                        <?php echo $model->form->editorFor("materials"); ?>
                    </div>
                     <div class="form-group">
                        <label>Description</label>
                        <?php echo $model->form->textAreaFor("description",['class'=>'ckeditor']); ?>
                    </div>
                    <div class="form-group">
                        <label>Metals -> Colors</label>
                        <?$metal_colors = $model->jewelry->metal_colors?json_decode($model->jewelry->metal_colors):json_decode(\Model\Jewelry::defaultMetalColors());
                        foreach($metal_colors as $metal=>$colors){?>
                            <div class="checkbox">
                                <label>
                                    <input class="metal" type="checkbox" value="<?=$metal?>" name="metals[]" <?=strpos($model->jewelry->metals, $metal) === false?'':'checked'?>><?=$metal?>
                                    <?foreach($colors as $color=>$val){?>
                                        <div class="checkbox">
                                            <label>
                                                <input class="color" type="checkbox" value="<?=$color?>" name="<?=$metal?>_colors[]" <?=$val == 0 ?'':'checked'?>><?=$color?>
                                            </label>
                                        </div>
                                    <?}?>
                                </label>
                            </div>
                        <?}?>
                    </div>

                    <div class="form-group">
                        <label>Jewelry Categories</label>
                        <?$categories = $model->jewelry->getCategories();
                        foreach($categories as $category){?>
                            <div class="checkbox">
                                <?=$category->name?>
                            </div>
                        <?}?>
                    </div>

                </div>
            </div>
          
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="seo-tab">
         <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>SEO</h4>
                    <div class="form-group">
                        <label>Slug</label>
                        <?php echo $model->form->editorFor("slug"); ?>
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                        <?php echo $model->form->editorFor("tags"); ?>
                    </div>
                    <div class="form-group">
                        <label>Meta Title</label>
                        <?php echo $model->form->editorFor("meta_title"); ?>
                    </div>
                     <div class="form-group">
                        <label>Meta Keywords</label>
                        <?php echo $model->form->editorFor("meta_keywords"); ?>
                    </div>
                     <div class="form-group">
                        <label>Meta Description</label>
                        <?php echo $model->form->editorFor("meta_description"); ?>
                    </div>
                </div>
            </div>
         </div>
      </div>
    
     
      <div role="tabpanel" class="tab-pane" id="categories-tab">
          <?$children=[]?>
          <?$noChildren=[]?>
          <?php foreach($model->categories as $cat) {if($cat->parent_category != 0){$children[$cat->id] = $cat->parent_category;}} ?>
        <select name="product_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
        <?php  foreach($model->categories as $cat) {
            foreach(array_unique($children) as $key => $value){
                if($value == $cat->id){?>
                    <optgroup label="<?=$cat->name?>">
                        <? foreach ($children as $catId => $parentCat) {
                            if($parentCat == $cat->id){?>
                                <option value="<?=$catId?>"><?echo \Model\Category::getItem($catId)->name?></option>
                            <?}
                        }?>
                    </optgroup>
                <?}
            }
            if($cat->parent_category == 0){
                array_push($noChildren, $cat->id);
            }
        } ?>
            <optgroup label="Parent Categories">
                <? foreach(array_values($noChildren) as $value){?>
                <option value="<?php echo $value;?>"><?php echo \Model\Category::getItem($value)->name;?></option>
            <?php } ?>
            </optgroup>


        </select>
      </div>

       
       
      <div role="tabpanel" class="tab-pane" id="images-tab">
        <div class="row">
          <div class="col-md-24">
            <div class="box">
        <div class="dropzone" id="dropzoneForm" action="<?php echo ADMIN_URL.'jewelry/upload_images/'.$model->jewelry->id;?>">
          
        </div>
        <button id="upload-dropzone" class="btn btn-danger">Upload</button><br />
        </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-24">
            <div class="box">
              <table id="image-container" class="table table-sortable-container">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>File Name</th>
                    <th>Display Order</th>
                    <th>Delete</th>
                  </tr>
                </thead>
             <tbody>
                 <?$jewelry = new \Model\Product_Image()?>
                  <?php foreach($jewelry->getProductImage($model->jewelry->id, "Jewelry") as $jImg) {

                    if($jImg->exists_image()) {
                    ?>
                  <tr data-image_id="<?php echo $jImg->id;?>">
                    <td><img src="<?php echo $jImg->get_image_url(); ?>" width="100" height="100" /></td>
                    <td><?php echo $jImg->image;?></td>
                    <td class="display-order-td"><?php echo $jImg->display_order;?></td>
                    <td class="text-center">
                      <a class="btn-actions delete-product-image" href="<?php echo ADMIN_URL; ?>jewelry/delete_prod_image/<?php echo $jImg->id; ?>?token_id=<?php echo get_token();?>">
                        <i class="icon-cancel-circled"></i> 
                      </a>
                    </td>
                  </tr>
                  <?php
                    }
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
          
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript">

    //	var colors = <?php //echo json_encode(explode(',', $model->ring->default_color)); ?>//;
    //  	var sizes = <?php //echo json_encode(explode(',', $model->product->sizes)); ?>//;
    var categories = <?php echo json_encode($model->product_categories); ?>;
    // var collections = <?php //echo json_encode($model->product_collections); ?>;
    // var materials = <?php //echo json_encode($model->product_materials); ?>;
    //console.log(categories);
    var site_url=<?php echo json_encode(ADMIN_URL.'rings/'); ?>;
    $(document).ready(function () {
        //var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
//    var video_thumbnail = new mult_image($("input[name='video_thumbnail']"),$("#preview-thumbnail-container"));

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });


        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
//  		  $("select[name='colors[]']").val(colors);
//  		  $("select[name='sizes[]']").val(sizes);
        $("select[name='product_category[]']").val(categories);
        //$("select[name='product_collection[]']").val(collections);
        //$("select[name='product_material[]']").val(materials);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-product-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post(site_url+'sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            }
        });
        $('.metal').on('change', function () {
            if ($(this).is(':checked')) {
                $(this).siblings().find('.color').prop('checked', true);
            } else {
                $(this).siblings().find('.color').prop('checked', false);
            }
        });
        $('.color').on('change', function () {
            if ($(this).is(':checked')) {
                $(this).parent().parent().siblings('.metal').prop('checked', true);
            }
        });

        $("input[name='featured']").on('click', function(){
            if($(this).is(':checked')){
                $("input[name='discount']").parent().show();
            } else {
                $("input[name='discount']").parent().hide();
                $("input[name='discount']").val(0);
            }
        });

    });
</script>
<script type="text/javascript">
Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

  // The configuration we've talked about above
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100,
  maxFiles: 100,
   url: <?php  echo json_encode(ADMIN_URL.'jewelry/upload_images/'.$model->jewelry->id);?>,
  // The setting up of the dropzone
  init: function() {
    var myDropzone = this;

    // First change the button to actually tell Dropzone to process the queue.
    $("#upload-dropzone").on("click", function(e) {
      // Make sure that the form isn't actually being sent.
      e.preventDefault();
      e.stopPropagation();
      myDropzone.processQueue();
    });

    // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
    // of the sending event because uploadMultiple is set to true.
    this.on("sendingmultiple", function() {
      // Gets triggered when the form is actually being sent.
      // Hide the success button or the complete form.
      $("#upload-dropzone").prop("disabled",true);
    });
    this.on("successmultiple", function(files, response) {
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
      window.location.reload();
      $("#upload-dropzone").prop("disabled",false);
    });
    this.on("errormultiple", function(files, response) {
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    });
  }

}
</script>