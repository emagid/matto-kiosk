<?php
    if(count($model->surveys)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="40%">Survey Title</th>
          <th width="30%" class="text-center">Edit</th>
          <th width="30%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->surveys as $obj){ ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>surveys/update/<?= $obj->id ?>"><?php echo $obj->title; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>surveys/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>surveys/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'surveys';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>