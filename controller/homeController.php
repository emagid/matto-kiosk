<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function locations(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function franchise(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function wheel(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function about(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function app(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    
}