<?php

namespace Model;

class Survey extends \Emagid\Core\Model {
    static $tablename = "survey";

    public static $fields  =  [
    	'title'=>['required'=>true],
        'questions',
        'display_order' => ['type'=>'number'],
        'background_color'
    ];
}
























