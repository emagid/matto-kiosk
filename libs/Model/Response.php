<?php
/**
 * Created by PhpStorm.
 * User: Garrett
 * Date: 9/19/17
 * Time: 4:22 PM
 */

namespace Model;

class Response extends \Emagid\Core\Model {
    public static $tablename = "response";

    public static $fields = [
        'question_id',
        'answer_id',
        'kiosk_id',
        'survey_id',
        'session_id',          
    ];
}