<?php 

namespace Model; 

class Page extends \Emagid\Core\Model {

	static $tablename = 'page'; 
	
	public static $fields =  [
		'title', 
		'slug',
		'description', 
		'featured_image',
		'meta_title', 
		'meta_keywords', 
		'meta_description',
		'date_modified',
		'content',
		'template'
	];
	
	public static $templates = [
		1=>"Video",
        2=>"Image",
		3=>"Survey",
		4=>"Iframe"
	];
}