<?php

namespace Model;

class Category extends \Emagid\Core\Model {

  	static $tablename = "category";

  	public static $fields = [ 
	    'description',
	    'name' => ['required'=>true],
	    'subtitle',
	    'parent_category',
	    'slug' => ['required'=>true,'unique'=>true],
	    'meta_title', 
		'meta_keywords', 
		'meta_description',
		'display_order' => ['type'=>'numeric'],
		'in_menu'
	];
  	
	static $relationships = [
		[
			'name'=>'product_category',
			'class_name' => '\Model\Product_Category',
			'local'=>'id',
			'remote'=>'category_id',
			'remote_related'=>'product_id',
			'relationship_type' => 'many'
		],
    ];

	public function getParent($id=null){
		$id = $id ? : $this->id;
		$child = self::getItem($id);
		$parent_category = $child->parent_category;
		$arr = [];
		while($parent_category != 0){
			$arr[] = $newHead = self::getItem($parent_category);
			$parent_category = intval($newHead->parent_category);
		}
		return $arr;
	}

	public static function getNested($parentId, $limitChildren = []){
		$where = "active = 1 AND parent_category = {$parentId} ";
		$cat = self::getList(['where'=>$where]);
		$cat_id = array_map(function($cc){return $cc->id;},(array)$cat);
		$categories = array_map(function($c){return Category::getItem($c);},array_intersect($cat_id,$limitChildren))?:$cat;
		foreach ($categories as $category) {
			$category->children = self::getNested($category->id, $limitChildren);
		}
		return $categories;
	}

	public static function reverseNested($nestedCategory,$arr = [],$val = null){
		if(count($nestedCategory) > 0) {
			foreach ($nestedCategory as $nested) {
				$arr[] = $nested;
				$val = self::reverseNested($nested->children, $arr, $val);
			}
		} else {
			return $arr;
		}
		return $val;
	}

	public function categoryChildren($id = null){
		$id = $id ? : $this->id;
		$arr = [];
		$category = self::getItem($id);
		$children = self::getList(['where'=>"parent_category = $category->id"]);
		foreach($children as $child){
//			$arr[] = $child;
			$id = $child->id;
			while($cat = self::getList(['where'=>"parent_category in ($id)"])){
				$newId = [];
				foreach($cat as $c){
					$arr[$child->name][] = $c->name;
					$newId[] = $c->id;
				}
				$id = implode(',',$newId);
			}
		}
		return $arr;
	}

	public function filteredCategoryChildren($id = null, $model = null){
		$id = $id ? : $this->id;
		$arr = [];
		$prodCatList = [];
		$model = ucfirst(strtolower($model));
		$modelName = "\\Model\\$model";
		$newMod = $modelName::getList(['where'=>"active = 1", 'orderBy'=>"id asc"]);
		foreach($newMod as $mod){
			$prodCatList = array_merge($prodCatList,array_map(function($obj){return $obj->name;},$mod->getCategories()));
		}
		$category = self::getItem($id);
		$children = self::getList(['where'=>"parent_category = $category->id"]);
		foreach($children as $child){
			$id = $child->id;
			while($cat = self::getList(['where'=>"parent_category in ($id)"])){
				$newId = [];
				foreach($cat as $c) {
					if (in_array($c->name,array_unique($prodCatList))) {
						$arr[$child->name][] = $c->name;
						$newId[] = $c->id;
					}
				}
				$id = implode(',',$newId);
			}
		}
		return $arr;
	}
	public static function array_flatten($array) {

		$return = array();
		foreach ($array as $key => $value) {
			if (is_array($value)){ $return = array_merge($return, self::array_flatten($value));}
			else {$return[$key] = $value;}
		}
		return $return;

	}

}