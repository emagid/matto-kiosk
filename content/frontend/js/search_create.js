$(document).ready(function() {
	var $window = $(this);



    var scrollTop = $window.scrollTop();
    var window_width = $window.width();
    fix_filter_col(scrollTop,window_width);
    $window.scroll(function(e){
        var scrollTop = $(this).scrollTop();
        var window_width = $window.width();
        fix_filter_col(scrollTop,window_width);
    });

	$(window).on('resize', function(){
	    var scrollTop = $window.scrollTop();
	    var window_width = $window.width();
	    fix_filter_col(scrollTop,window_width);
	});    

	$(document).on('click','div#toggle_filter_container',function(event){
		if($(".search_tools_scroll_wrapper").hasClass("results_header_fixed")){
			$("body.browse_diamond div#primary_page_controller_wrapper > .content_wrapper_97").removeClass('filtersInactive').toggleClass('filtersActive');
		}else{
			$("body.browse_diamond div#primary_page_controller_wrapper > .content_wrapper_97").removeClass('filtersActive').toggleClass('filtersInactive');
		}
	});

    function fix_filter_col(scrollTop,window_width){
    	console.log(scrollTop);
    	var $search_tools = $(".search_tools_scroll_wrapper");
    	var $search_results_bottom = $(".search_results_col_right");
    	var search_tools_header_height = $(".search_tools_container .diagonal_pattern").height();
    	var $search_results_header = $("#diamond-table_wrapper .dataTables_scrollHead");
    	var $search_results_body = $("#diamond-table_wrapper .dataTables_scrollBody");
    	var search_results_header_height = $search_results_header.height();
    	var search_tools_height = $(".search_tools_container").height();
    	var fixResultsHeader = 94 + search_tools_height - search_tools_header_height - search_results_header_height;
    	console.log(fixResultsHeader);
        if(scrollTop >= 35){
            $search_tools.addClass("search_tools_fixed");
            $search_results_bottom.css({'margin-top':search_tools_height+'px'}).addClass('marginTopActive');
        } else{
            $search_tools.removeClass("search_tools_fixed");
            $search_results_bottom.css({'margin-top':'0px'}).removeClass('marginTopActive');
        }

        if(scrollTop >= fixResultsHeader){
        	$search_results_header.addClass("search_results_header_fixed");
        	$search_tools.addClass("results_header_fixed");
        	$search_results_body.css({'margin-top':search_results_header_height+'px'});
        }else{
        	$search_results_header.removeClass("search_results_header_fixed");
        	$search_tools.removeClass("results_header_fixed");
        	$search_results_body.css({'margin-top':'0px'});
        }
    }

});

